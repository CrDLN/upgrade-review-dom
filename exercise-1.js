/*1.1 Basandote en el array siguiente, crea una lista ul > li 
dinámicamente en el html que imprima cada uno de los paises.*/
const countries = ['Japón', 'Nicaragua', 'Suiza', 'Australia', 'Venezuela'];

var elementLu = document.createElement('lu');

for (const key in countries) {
    var newli = document.createElement('li');
    newli.innerText= countries[key];
    elementLu.appendChild(newli);
}

var currentNode = document.querySelector('div1');

document.body.insertBefore(elementLu,currentNode);

/*1.2 Elimina el elemento que tenga la clase .fn-remove-me.*/

var elementFnRemoveMe = document.querySelector(".fn-remove-me").remove();

/*1.3 Utiliza el array para crear dinamicamente una lista ul > li de elementos 
en el div de html con el atributo data-function="printHere".*/
const cars = ['Mazda 6', 'Ford fiesta', 'Audi A4', 'Toyota corola'];

var elementLuTwo = document.createElement('lu');

for (const llave in cars) {
    var newliTwo = document.createElement('li');
    newliTwo.innerText= cars[llave];
    elementLuTwo.appendChild(newliTwo);
}
var vPrintHere = document.body.querySelector('[data-function="printHere"]');
vPrintHere.appendChild(elementLuTwo);

document.body.insertBefore(vPrintHere,currentNode);


/*1.4 Crea dinamicamente en el html una lista de div que contenga un elemento 
h4 para el titulo y otro elemento img para la imagen.*/
const countriesImg = [
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=1'}, 
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=2'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=3'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=4'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=5'}
];

//var currentNode = document.querySelector('div');

for (const key in countriesImg) {
    var elementDiv = document.createElement('div');

    var tittleHfor= document.createElement('h4');
    tittleHfor.innerText = countriesImg[key].title;

    var image = document.createElement('img');
    image.src= countriesImg[key].imgUrl;

    elementDiv.appendChild(tittleHfor);
    elementDiv.appendChild(image);
    document.body.insertBefore(elementDiv,currentNode);
}

/*1.5 Basandote en el ejercicio anterior. Crea un botón que elimine el último 
elemento de la lista.*/

//Creación de boton
var btn = document.createElement('button');
btn.id='btnRemoveDiv';
btn.innerText = 'Borrar último elemento';
document.body.insertBefore(btn,currentNode);


var btnRemove = document.querySelector('#btnRemoveDiv').addEventListener('click', removeLastElementList);

function removeLastElementList (){
    var elementDivRemove = document.querySelectorAll('div');
    var lastChild = elementDivRemove[elementDivRemove.length-1];
    lastChild.remove()
}


/*1.6 Basandote en el ejercicio anterior. Crea un botón para cada uno de los 
elementos de las listas que elimine ese mismo elemento del html.*/

var elements = document.querySelectorAll('div')
console.log(elements);

for (const elm in elements) {
    var btnElement = createButton();
    
    /**Error de código */
    elements[elm].appendChild(btnElement);
}



function removeThisElement(event){
    this.parentNode.remove()
}

function createButton(){
    var btnElement = document.createElement('button');
    btnElement.innerText = "Borrar";
    btnElement.id = 'btnRemoveThis';
    btnElement.addEventListener('click', removeThisElement);

    return btnElement;
}

//he cambiado el nombre del segundo array ya que daba 
//problemas con el segundo si quería meterlo todo en el mismo archivo